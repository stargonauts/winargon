from __future__ import division, print_function
import sys

try:
    import requests
except (ImportError):
    print("ERROR: this requires the requests module")
    print("Install it using 'pip install requests'")
    sys.exit(1)

USER = "stargonauts"
REPO = "argon"

FILES = [
    "platform",
    "platform_OF",
    "utilities",
    "mdforces",
    "potentials",
    "gaussian",
    "cubicspline",
    "audio",
    "gui_base",
    "gui_derived"
]

FILES_CPP = [
    "ofApp",
    "main",
    "gui_derived_potential",
    "gui_derived_system",
    "gui_derived_tutorial"
]

FILES_H = [
    "info_text",
    "ofApp"
]

BB_API = "https://api.bitbucket.org/2.0/repositories/{}/{}".format(USER, REPO)
BB_URL = "https://bitbucket.org/{}/{}".format(USER, REPO)

if (len(sys.argv) > 1):
    branch = sys.argv[1]
else:
    branch = "master"

master = requests.get("{}/refs/branches/{}".format(BB_API, branch))

if (master.status_code != 200):
    print("Error fetching branch '{}'".format(branch))
    sys.exit(master.status_code)

hashkey = master.json()["target"]["hash"]
printstr = "Fetching files from commit {}:".format(hashkey)

def download_file(name, ext_in, ext_out):
    """ downloads the file called 'name.ext_in' to 'src/name.ext_out"""
    furl = requests.get("{}/raw/{}/src/{}.{}".format(BB_URL, hashkey, name, ext_in))

    if (furl.status_code != 200):
        print("\nError downloading '{}.{}'".format(name, ext_in))
        sys.exit(furl.status_code)

    with open("src/{}.{}".format(name, ext_out), 'wb') as fobj:
        fobj.write(furl.content)

def print_progress(printstr, progress):
    sys.stdout.write("\r{} {:.2%}".format(printstr, progress))
    sys.stdout.flush()

i = 0
total = len(FILES) * 2 + len(FILES_H) + len(FILES_CPP)

print_progress(printstr, 0)

for name in FILES:
    download_file(name, "hpp", "hpp")
    i += 1
    print_progress(printstr, i / total)

for name in FILES_H:
    download_file(name, "h", "h")
    i += 1
    print_progress(printstr, i / total)

for name in (FILES + FILES_CPP):
    download_file(name, "cpp", "cpp")
    i += 1
    print_progress(printstr, i / total)

print_progress(printstr, 1)
print()
